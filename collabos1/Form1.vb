﻿Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'First we check if text is actually a number
        If Not IsNumeric(TextBox1.Text) Then
            MsgBox(TextBox1.Text & " is not a number")
        Else
            'now that we have a number lets check if its bigger than 0
            If Val(TextBox1.Text) > 0 Then
                Dim rez As Double = 1
                For i As Integer = 1 To Val(TextBox1.Text) Step 1
                    rez = rez * i
                Next
                Label1.Text = rez
            Else
                Label1.Text = 0
            End If
        End If
    End Sub
End Class
